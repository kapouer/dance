# Salsa transition dance

This tool helps to fetch failed job logs, and retry logs of jobs when they match a given string.

![Screenshot of fetch](terminal.png)

## Usage

```sh
./dance fetch -p <team> -l <pipelineId>
./dance retry -p <team> -l <pipelineId>
./dance report
```

Pass `--yes` to skip prompts.

## Config

Prepare a config.toml with:

```toml
logs = "logs"
jobPrefix = "build-rdep-"
retryReason = "failed because the control process exited with error code"
baseUrl = "https://salsa.debian.org/api/v4/projects/"
token = "<salsa r/w api access token>"
udd = "postgresql://guest@localhost:5452/udd"
bugTitle = "FTBFS with nodejs experimental/14.19.0"
```

## udd

One needs up-to-date information about bugs, so until i figured how
to use cgi to get the information, doing a ssh proxy of udd to localhost is the way.

## See also

[Salsa transition scripts](https://salsa.debian.org/js-team/nodejs/-/tree/master-14.x/debian)
