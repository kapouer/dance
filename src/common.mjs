export function parseFilename(filename) {
	const {
		groups: {
			source,
			job
		}
	} = /^(?<source>[a-z][a-z.-\d]+)-(?<job>\d+)\.log$/.exec(filename) || {
		groups: {}
	};
	if (Number.isNaN(parseInt(job))) return {};
	return { source, job };
}
