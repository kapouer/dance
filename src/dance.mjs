#!/usr/bin/node

import { readFile } from 'node:fs/promises';
import got from 'got';
import toml from 'toml';
import Args from 'command-line-args';
import fetchLogs from './fetch.mjs';
import retryJobs from './retry.mjs';
import reportFailures from './report.mjs';

const main = Args([{
	name: 'command',
	defaultOption: true
}], {
	stopAtFirstUnknown: true
});
const argv = main._unknown || [];

const opts = Args([
	{
		name: 'project',
		alias: 'p',
		type: String
	}, {
		name: 'pipeline',
		alias: 'l',
		type: Number
	}, {
		name: 'yes',
		type: Boolean
	}
], { argv });

const config = toml.parse(await readFile('config.toml'));

config.call = got.extend({
	retry: { limit: 0 },
	headers: {
		Authorization: `Bearer ${config.token}`
	}
});

const baseUrl = new URL(config.baseUrl);
baseUrl.pathname += encodeURIComponent(opts.project);
baseUrl.pathname += "/";
config.baseUrl = baseUrl;

switch (main.command) {
	case "fetch":
		await fetchLogs(config, opts);
		break;
	case "retry":
		await retryJobs(config, opts);
		break;
	case "report":
		await reportFailures(config, opts);
		break;
	default:
		console.error("Unknown command", main.command);
		process.exit(1);
}
