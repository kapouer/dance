import stream from 'node:stream';
import { promisify } from 'node:util';
const pipeline = promisify(stream.pipeline);
import { mkdir, stat } from 'node:fs/promises';
import path from 'node:path';
import { createWriteStream } from 'node:fs';

export default async function fetchFailedJobLogs(config, opts) {
	const pipelineJobsUrl = new URL(`pipelines/${opts.pipeline}/jobs`, config.baseUrl);
	pipelineJobsUrl.searchParams.set('scope', 'pending');
	pipelineJobsUrl.searchParams.set('per_page', '1');

	const { headers } = await config.call(pipelineJobsUrl);

	console.info(headers["x-total"], "pending jobs");

	pipelineJobsUrl.searchParams.set('scope', 'failed');
	// pipelineJobsUrl.searchParams.set('include_retried', 'false');
	pipelineJobsUrl.searchParams.set('per_page', '100');

	const list = (await config.call(pipelineJobsUrl).json()).filter(job => {
		return job.name.startsWith(config.jobPrefix);
	});

	console.info(list.length, "failed jobs");

	// ok so we have the list of failed jobs
	// now we want to collect all failed logs

	if (list.length) await mkdir(config.logs, { recursive: true });

	for (const job of list) {
		const name = job.name.replace(config.jobPrefix, '');
		const filename = path.join(config.logs, `${name}-${job.id}.log`);
		try {
			await stat(filename);
			console.info("already fetched", name);
		} catch (ex) {
			const joblogUrl = new URL(`jobs/${job.id}/trace`, config.baseUrl);
			await pipeline(
				config.call.stream(joblogUrl),
				createWriteStream(filename)
			);
			console.info("fetched", name);
		}
	}
}
