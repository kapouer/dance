import { readdir, readFile, unlink } from 'node:fs/promises';
import assert from 'node:assert';
import path from 'node:path';
import prompts from 'prompts';
import { parseFilename } from './common.mjs';

export default async function retryJobs(config, opts) {
	const files = await readdir(config.logs);
	const retries = [];
	for (const name of files) {
		const filename = path.join(config.logs, name);
		const { job } = parseFilename(name);
		if (job == null) continue;
		try {
			const txt = await readFile(filename, { encoding: 'utf-8' });
			assert.ok(txt.indexOf(config.retryReason) >= 0);
		} catch (ex) {
			continue;
		}
		retries.push({ job, name, filename });
	}
	if (retries.length) {
		const retry = opts.yes || (await prompts({
			type: 'confirm',
			name: 'yes',
			message: `Retry ${retries.length} jobs now ?`,
			initial: false
		})).yes;
		if (retry) {
			for (const { job, name, filename } of retries) {
				const jobretryUrl = new URL(`jobs/${job}/retry`, config.baseUrl);
				await config.call.post(jobretryUrl);
				await unlink(filename);
				console.info("retried", job, name);
			}
		}
	} else {
		console.info("no job to retry");
	}
}
