
import { promisify } from 'node:util';
import child_process from 'node:child_process';
import path from 'node:path';
import { readdir, readFile } from 'node:fs/promises';
import assert from 'node:assert';
import prompts from 'prompts';
import knex from 'knex';

import { parseFilename } from './common.mjs';

const exec = promisify(child_process.exec);

export default async function reportFailures(config, opts) {
	if (!config.bugTitle) {
		console.error("Missing bugTitle in config file");
		process.exit(1);
	}
	const db = knex({
		client: 'pg',
		connection: config.udd
	});

	const files = await readdir(config.logs);
	const reports = [];

	for (const name of files) {
		const filename = path.join(config.logs, name);
		const { job, source } = parseFilename(name);
		if (job == null) continue;
		try {
			const txt = await readFile(filename, { encoding: 'utf-8' });
			assert.ok(txt.indexOf(config.retryReason) < 0);
		} catch (ex) {
			continue;
		}
		const bugs = await getFTBFS(db, source);
		if (bugs.some(bug => bug.title.indexOf(config.bugTitle) >= 0)) {
			// same title, continue
			console.info("Skip already submitted report for", source);
			continue;
		}
		let reportBug = true;
		if (bugs.length) {
			console.info(
				`-- ${source} --\n already has a bug tagged ftbfs: \n ${bugs.map(item => item.id + ': ' + item.title).join('\n ')}`
			);
			reportBug = opts.yes || (await prompts({
				name: 'yes',
				type: 'confirm',
				message: 'Add a FTBFS report anyway ?',
				initial: false
			})).yes;
		}
		if (reportBug) reports.push({ source, filename });
	}
	await db.destroy();

	if (reports.length) {
		const send = opts.yes || (await prompts({
			type: 'confirm',
			name: 'yes',
			message: `Send FTBFS reports to ${reports.map(item => item.source).join(', ')} now ?`,
			initial: false
		})).yes;
		if (send) {
			for (const item of reports) {
				await report(config, item);
			}
			console.info("Reports sent");
		}
	}
}

async function getFTBFS(db, name) {
	return db('bugs')
		.where({
			source: name
		})
		.join('bugs_tags', {
			'bugs_tags.id': 'bugs.id'
		})
		.where('bugs_tags.tag', 'ftbfs')
		.select('bugs.id', 'bugs.title');
}
async function report(config, { source, filename }) {
	// query bts to make sure a package has not already some ftbfs tag
	const com = [
		'reportbug',
		'--severity=important',
		'--tag=ftbfs',
		'--body="This package fails to rebuild.\nBuild log is attached."',
		'--no-bug-script',
		'--no-cc-menu',
		'--no-tags-menu',
		'--no-verify',
		'--no-cc',
		'--no-query-bts',
		`--attach=${filename}`,
		`--subject="${config.bugTitle}"`,
		'--source',
		source
	].join(' ');
	const { stdout, stderr } = await exec(com);
	if (stdout) console.info(stdout);
	if (stderr) console.error(stderr);
}
